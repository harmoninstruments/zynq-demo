#!/usr/bin/env python3
# Copyright (C) 2024 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

from devtool import Devtool, default_host
import argparse

def auto_int(x):
    return int(x, 0)

parser = argparse.ArgumentParser()
parser.add_argument('--host', type=str, help="ip or hostname of device", default=default_host)
parser.add_argument('--program_file', type=str, help="filename to program", default=None)
parser.add_argument('--read_file', type=str, help="filename to read to", default=None)
parser.add_argument('--address', type=auto_int, help="address in QSPI", default=0)
parser.add_argument('--length', type=auto_int, help="number of bytes to read", default=65536)
args = parser.parse_args()
d = Devtool(args.host)

if args.program_file is not None:
    d.write_file_qspi(args.program_file, args.address)

if args.read_file is not None:
    data = d.read_qspi(args.address, args.length)
    with open(args.read_file, "wb") as f:
        f.write(data)
