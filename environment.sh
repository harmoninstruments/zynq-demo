#!/bin/bash

XILINX=/opt/Xilinx

source ${XILINX}/Vivado/2023.2/settings64.sh
source ${XILINX}/Vitis/2023.2/settings64.sh
export SOURCE_DATE_EPOCH=$(git log -1 --pretty=%ct)
