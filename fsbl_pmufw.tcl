# run with xsct Reference: UG1400 Ch. 28, 29

hsi::open_hw_design "build_bd/demo.gen/sources_1/bd/bd_ps/synth/bd_ps.hwdef"
hsi::generate_app -os standalone -proc "psu_cortexa53_0" -app "zynqmp_fsbl" -dir "build_fsbl"
hsi::generate_app -os standalone -proc "psu_pmu_0" -app "zynqmp_pmufw" -sw pmufw -dir "build_pmufw"
hsi::generate_app -os standalone -proc "psu_cortexr5_0" -app "hello_world" -dir "build_app_r5_0"
hsi::generate_app -os freertos10_xilinx -proc "psu_cortexa53_0" -app "freertos_lwip_tcp_perf_server" -dir "build_app_lwip"
#hsi::generate_app -os standalone -proc "psu_cortexa53_0" -app "lwip_echo_server" -dir "build_app_lwip"
hsi::generate_bsp -os standalone -proc "psu_cortexr5_0" -dir "build_bsp_r5_0"


# Cortex A53-0 debug BSP
set sw_a53_0 [hsi::create_sw_design a53_0 -proc psu_cortexa53_0 -os freertos10_xilinx ]
# FPGA configuration, xilsecure is dependency
hsi::add_library xilsecure
hsi::add_library xilfpga
#Property                    Type    Read-only  Value
#CONFIG.base_address         string  false      0x80000
#CONFIG.data_readback_en     string  false      true
#CONFIG.debug_mode           string  false      false
#CONFIG.get_feature_list_en  string  false      false
#CONFIG.get_version_info_en  string  false      false
#CONFIG.ocm_address          string  false      0xfffc0000
#CONFIG.reg_readback_en      string  false      true
#CONFIG.secure_environment   string  false      false
#CONFIG.secure_mode          string  false      true
#CONFIG.secure_readback      string  false      false
#CONFIG.skip_efuse_check_en  string  false      false
#NAME                        string  true       xilfpga
common::report_property [hsi::get_libs xilfpga]
# FAT filesystem
hsi::add_library xilffs
set lib [hsi::get_libs xilffs]
common::report_property [hsi::get_libs xilffs]
#Property                       Type    Read-only  Value
#CONFIG.enable_exfat            string  false      false
common::set_property CONFIG.enable_multi_partition "true" $lib
#CONFIG.fs_interface            string  false      1
#CONFIG.num_logical_vol         string  false      2
#CONFIG.ramfs_size              string  false      3145728
#CONFIG.ramfs_start_addr        string  false
#CONFIG.read_only               string  false      false
#CONFIG.set_fs_rpath            string  false      0
#CONFIG.use_chmod               string  false      false
#CONFIG.use_lfn                 string  false      0
#CONFIG.use_mkfs                string  false      true
#CONFIG.use_strfunc             string  false      0
#CONFIG.use_trim                string  false      false
#CONFIG.word_access             string  false      true

# network stack
hsi::add_library lwip213
set lib [hsi::get_libs lwip213]
#Property                              Type    Read-only  Value
common::set_property CONFIG.api_mode "SOCKET_API" $lib
#CONFIG.arp_options                    string  false      true
#CONFIG.arp_queueing                   string  false      1
common::set_property CONFIG.arp_table_size "10" $lib
#CONFIG.debug_options                  string  false      true
#CONFIG.default_tcp_recvmbox_size      string  false      200
#CONFIG.default_udp_recvmbox_size      string  false      100
#CONFIG.dhcp_does_arp_check            string  false      false
#CONFIG.dhcp_options                   string  false      true
#CONFIG.emac_number                    string  false      0
#CONFIG.icmp_debug                     string  false      false
#CONFIG.icmp_options                   string  false      true
#CONFIG.icmp_ttl                       string  false      255
#CONFIG.igmp_debug                     string  false      false
#CONFIG.igmp_options                   string  false      false
#CONFIG.ip_debug                       string  false      false
#CONFIG.ip_default_ttl                 string  false      255
#CONFIG.ip_forward                     string  false      0
#CONFIG.ip_frag                        string  false      1
#CONFIG.ip_frag_max_mtu                string  false      1500
#CONFIG.ip_options                     string  false      0
#CONFIG.ip_reass_max_pbufs             string  false      128
#CONFIG.ip_reassembly                  string  false      1
#CONFIG.ipv6_enable                    string  false      false
#CONFIG.ipv6_options                   string  false      true
#CONFIG.lwip_debug                     string  false      false
common::set_property CONFIG.lwip_dhcp "true" $lib
#CONFIG.lwip_ip_options                string  false      true
#CONFIG.lwip_memory_options            string  false
#CONFIG.lwip_stats                     string  false      false
#CONFIG.lwip_tcp                       string  false      true
#CONFIG.lwip_tcp_keepalive             string  false      false
#CONFIG.lwip_tcpip_core_locking_input  string  false      false
#CONFIG.lwip_udp                       string  false      true
#CONFIG.mbox_options                   string  false      true
#CONFIG.mem_size                       string  false      131072
#CONFIG.memp_n_pbuf                    string  false      16
#CONFIG.memp_n_sys_timeout             string  false      8
#CONFIG.memp_n_tcp_pcb                 string  false      32
#CONFIG.memp_n_tcp_pcb_listen          string  false      8
#CONFIG.memp_n_tcp_seg                 string  false      256
#CONFIG.memp_n_udp_pcb                 string  false      4
#CONFIG.memp_num_api_msg               string  false      16
#CONFIG.memp_num_netbuf                string  false      8
#CONFIG.memp_num_netconn               string  false      16
#CONFIG.memp_num_tcpip_msg             string  false      64
#CONFIG.n_rx_coalesce                  string  false      1
#CONFIG.n_rx_descriptors               string  false      64
#CONFIG.n_tx_coalesce                  string  false      1
#CONFIG.n_tx_descriptors               string  false      64
#CONFIG.netif_debug                    string  false      false
#CONFIG.no_sys_no_timers               string  false      true
#CONFIG.pbuf_debug                     string  false      false
#CONFIG.pbuf_link_hlen                 string  false      16
#CONFIG.pbuf_options                   string  false      true
#CONFIG.pbuf_pool_bufsize              string  false      1700
#CONFIG.pbuf_pool_size                 string  false      256
#CONFIG.phy_link_speed                 string  false      CONFIG_LINKSPEED_AUTODETECT
#CONFIG.sgmii_fixed_link               string  false      false
#CONFIG.socket_debug                   string  false      false
#CONFIG.socket_mode_thread_prio        string  false      2
#CONFIG.stats_options                  string  false      true
#CONFIG.sys_debug                      string  false      false
#CONFIG.tcp_debug                      string  false      false
#CONFIG.tcp_ip_rx_checksum_offload     string  false      false
#CONFIG.tcp_ip_tx_checksum_offload     string  false      false
#CONFIG.tcp_maxrtx                     string  false      12
#CONFIG.tcp_mss                        string  false      1460
#CONFIG.tcp_options                    string  false      true
#CONFIG.tcp_queue_ooseq                string  false      1
#CONFIG.tcp_rx_checksum_offload        string  false      false
#CONFIG.tcp_snd_buf                    string  false      8192
#CONFIG.tcp_synmaxrtx                  string  false      4
#CONFIG.tcp_ttl                        string  false      255
#CONFIG.tcp_tx_checksum_offload        string  false      false
#CONFIG.tcp_wnd                        string  false      2048
#CONFIG.tcpip_mbox_size                string  false      200
#CONFIG.temac_adapter_options          string  false      true
#CONFIG.temac_use_jumbo_frames         string  false      false
#CONFIG.udp_debug                      string  false      false
#CONFIG.udp_options                    string  false      true
#CONFIG.udp_ttl                        string  false      255
#CONFIG.udp_tx_blocking                string  false      false
#CONFIG.use_axieth_on_zynq             string  false      1
#CONFIG.use_emaclite_on_zynq           string  false      1

common::report_property [hsi::get_libs lwip213]
hsi::generate_bsp -dir "build_bsp_a53"

# generate a sytem device tree (not used here)
sdtgen set_dt_param -dir sdt -xsa zynqmp.xsa
sdtgen generate_sdt
