create_project demo build_bd -part xck26-sfvc784-2LV-c
set_property board_part xilinx.com:k26c:part0:1.3 [current_project]
create_bd_design "bd_ps"
update_compile_order -fileset sources_1

create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e zynqps

apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynqps]

set_property -dict [list \
                        CONFIG.PSU__PSS_ALT_REF_CLK__FREQMHZ {50.0} \
                        CONFIG.PSU__VIDEO_REF_CLK__ENABLE {0} \
                        CONFIG.PSU__PSS_ALT_REF_CLK__ENABLE {1} \
                        CONFIG.PSU__ENET1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__ENET1__GRP_MDIO__ENABLE {1} \
                        CONFIG.PSU__I2C0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__I2C0__PERIPHERAL__IO {EMIO} \
                        CONFIG.PSU__USB0__REF_CLK_SEL {Ref Clk1} \
                        CONFIG.PSU__USB1__REF_CLK_SEL {Ref Clk1} \
                        CONFIG.PSU__USB0__REF_CLK_FREQ {100} \
                        CONFIG.PSU__USB1__REF_CLK_FREQ {100} \
                        CONFIG.PSU__DP__LANE_SEL {Single Lower} \
                        CONFIG.PSU__UART0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__UART0__PERIPHERAL__IO {MIO 50 .. 51} \
                        CONFIG.PSU__UART1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__UART1__PERIPHERAL__IO {EMIO} \
                        CONFIG.PSU__USB0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB__RESET__MODE {Disable} \
                        CONFIG.PSU__USB1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB3_0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB3_0__PERIPHERAL__IO {GT Lane2} \
                        CONFIG.PSU__USB3_1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__FPGA_PL0_ENABLE {0} \
                        CONFIG.PSU__FPGA_PL1_ENABLE {0} \
                        CONFIG.PSU__USE__FABRIC__RST {0} \
                        CONFIG.PSU__SD0__PERIPHERAL__ENABLE {1}  \
                        CONFIG.PSU__SD0__PERIPHERAL__IO {MIO 13 .. 22}  \
                        CONFIG.PSU__SD0__GRP_CD__ENABLE {0}  \
                        CONFIG.PSU__SD0__GRP_POW__ENABLE {1}  \
                        CONFIG.PSU__SD0__GRP_POW__IO {MIO 23}  \
                        CONFIG.PSU__SD0__GRP_WP__ENABLE {0}  \
                        CONFIG.PSU__SD0__SLOT_TYPE {eMMC}  \
                        CONFIG.PSU__SD0__RESET__ENABLE {1}  \
                        CONFIG.PSU__SD0__DATA_TRANSFER_MODE {8Bit}  \
                        CONFIG.PSU__DDRC__CWL {16} \
                        CONFIG.PSU_MIO_70_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_70_SLEW {slow} \
                        CONFIG.PSU_MIO_71_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_71_SLEW {slow} \
                        CONFIG.PSU_MIO_72_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_72_SLEW {slow} \
                        CONFIG.PSU_MIO_73_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_73_SLEW {slow} \
                        CONFIG.PSU_MIO_74_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_74_SLEW {slow} \
                        CONFIG.PSU_MIO_75_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_75_SLEW {slow} \
                        CONFIG.PSU__DISPLAYPORT__PERIPHERAL__ENABLE {0} \
                        CONFIG.PSU__USE__M_AXI_GP0 {1} \
                        CONFIG.PSU__USE__M_AXI_GP1 {0} \
                        CONFIG.PSU__USE__M_AXI_GP2 {1} \
                        CONFIG.PSU__MAXIGP2__DATA_WIDTH {128} \
                        CONFIG.PSU__USE__S_AXI_GP2 {1} \
                        CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__SRCSEL {DPLL} \
                        CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__SRCSEL {VPLL} \
                        CONFIG.PSU__CRL_APB__CPU_R5_CTRL__SRCSEL {IOPLL} \
                        CONFIG.PSU__CRF_APB__ACPU_CTRL__FREQMHZ {1325} \
                        CONFIG.PSU__CRF_APB__GPU_REF_CTRL__FREQMHZ {100} \
                        CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__FREQMHZ {300} \
                        CONFIG.PSU__CRF_APB__APLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRF_APB__DPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRF_APB__VPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRL_APB__IOPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRL_APB__RPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK}] [get_bd_cells zynqps]

make_bd_pins_external [get_bd_cells zynqps]
make_bd_intf_pins_external  [get_bd_cells zynqps]

assign_bd_address -target_address_space /zynqps/Data [get_bd_addr_segs M_AXI_HPM0_FPD_0/Reg] -force
# this doesn't seem to matter except for generated headers. All ranges listed in the TRM work.
set_property range 256M [get_bd_addr_segs {zynqps/Data/SEG_M_AXI_HPM0_FPD_0_Reg}]
set_property offset 0x00A0000000 [get_bd_addr_segs {zynqps/Data/SEG_M_AXI_HPM0_FPD_0_Reg}]

save_bd_design

generate_target all [get_files bd_ps.bd]
catch { config_ip_cache -export [get_ips -all bd_ps_zynqps_0] }
export_ip_user_files -of_objects [get_files bd_ps.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] bd_ps.bd]
launch_runs bd_ps_zynqps_0_synth_1 -jobs 16
wait_on_run bd_ps_zynqps_0_synth_1
close_project
exit
#-----------------------------------------------------------
# Vivado v2023.2 (64-bit)
# SW Build 4029153 on Fri Oct 13 20:13:54 MDT 2023
# IP Build 4028589 on Sat Oct 14 00:45:43 MDT 2023
# SharedData Build 4025554 on Tue Oct 10 17:18:54 MDT 2023
# Start of session at: Mon Jan  1 21:24:40 2024
# Process ID: 388687
# Current directory: /home/dlharmon
# Command line: vivado
# Log file: /home/dlharmon/vivado.log
# Journal file: /home/dlharmon/vivado.jou
# Running On: dlhdesktop, OS: Linux, CPU Frequency: 4690.627 MHz, CPU Physical cores: 32, Host memory: 66510 MB
#-----------------------------------------------------------
start_gui
xhub::refresh_catalog [xhub::get_xstores xilinx_board_store]
set_param board.repoPaths {/home/dlharmon/.Xilinx/Vivado/2023.2/xhub/board_store/xilinx_board_store}
xhub::install [xhub::get_xitems avnet.com:xilinx_board_store:Ultra96v2:1.2]
create_project project_1 /home/dlharmon/project_1 -part xczu3eg-sbva484-1-i
set_property board_part avnet.com:ultra96v2:part0:1.2 [current_project]
create_bd_design "design_1"
update_compile_order -fileset sources_1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.5 zynq_ultra_ps_e_0
endgroup
apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]
write_hw_platform -fixed -force -file hi_kria.xsa
