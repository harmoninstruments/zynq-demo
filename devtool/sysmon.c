// Copyright (C) 2024 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include "stdio.h"
#include "sysmon_regs.h"
#include "xil_io.h"
#include "stdint.h"
#include "sleep.h"

static inline void out16(size_t a, uint16_t d) {Xil_Out16(a,d);}
static inline uint16_t in16(size_t a) {return Xil_In16(a);}

int sysmon_init_ps(void) {
        // TRM UG1085
        // example - continuous loop mode
        // should check jtag_locked in MON_STATUS first
        out16(AMS_PS_SYSMON_CONFIG_REG1, 0);
        out16(AMS_PS_SYSMON_CONFIG_REG2, 0);
        out16(AMS_PS_SYSMON_CONFIG_REG3, 0);
        out16(AMS_PL_SYSMON_CONFIG_REG1, 0);
        out16(AMS_PL_SYSMON_CONFIG_REG2, 0);
        out16(AMS_PL_SYSMON_CONFIG_REG3, 0);

        // we might should reset, initilize here but
        // FSBL should have already done it for us
        //Xil_Out32(AMS_CTRL_PS_SYSMON_CONTROL_STATUS, 0); // release reset

        out16(AMS_PL_SYSMON_ANALOG_BUS, 0x3210);

        // enable channels see TRM table 9-2
        out16(AMS_PL_SYSMON_SEQ_AVERAGE0, (1<<8) | (1<<0));
        out16(AMS_PL_SYSMON_SEQ_CHANNEL0, 1);
        out16(AMS_PL_SYSMON_SEQ_CHANNEL1, 0b0100111111100001);
        out16(AMS_PL_SYSMON_SEQ_CHANNEL2, 0); // AUX
        out16(AMS_PS_SYSMON_SEQ_AVERAGE0, (1<<8) | (1<<0));
        out16(AMS_PS_SYSMON_SEQ_CHANNEL0,
                  (1<<14) | (1<<10) | (1<<9) | (1<<8) | (1<<7) | (1<<6) | (1<<5) | (1<<0));
        out16(AMS_PS_SYSMON_SEQ_CHANNEL2, 0x1F);

        // enable continuous sequence
        out16(AMS_PS_SYSMON_CONFIG_REG1, 0x200);
        out16(AMS_PL_SYSMON_CONFIG_REG1, 0x200);
}

// print a voltage from sysmon with scaling
// scale = 8000 * input range in volts
static void print_v_scale(unsigned reg, const char *name, unsigned scale){
        unsigned int raw = in16(reg);
        raw *= scale;
        raw /= 524288;
        unsigned int before_decimal = raw / 1000;
        unsigned int after_decimal = raw % 1000;
	xil_printf("%s: %0d.%03d V\r\n", name, before_decimal, after_decimal);
}

static inline void print_supply(u32 reg, char* name) {
        print_v_scale(reg, name, 24000);
}

static inline void print_supply_som(u32 reg, char* name) {
        // 88000 would cause integer overflow
        // for very high SOM input voltage but
        // the SOM would be dead if that was the case
        print_v_scale(reg, name, 88000);
}

static inline void print_supply6(u32 reg, char* name) {
        print_v_scale(reg, name, 48000);
}

// reads sysmon, returns an integer in microdegrees C
// avoiding floating point for this thread
static inline int get_temp(u32 reg) {
        // degrees C = raw * (509.3140064f/65536.0f) - 280.23087870f;
        int raw = in16(reg);
        raw *= 15543; // 2e6*509.314/65536 (approximate)
        raw -= 560461757; // 2e6*280.2308 (approximate)
        return raw / 2;
}

void print_temp(u32 reg, char* name) {
        int tempc = get_temp(reg) + 300000000; // add 300 C so it's positive
        int before_decimal = tempc/1000000 - 300;
        int after_decimal = (tempc%1000000 + 5000)/10000;
        xil_printf("%s: %0d.%02d C\r\n", name, before_decimal, after_decimal);
}

int sysmon(void)
{
        sysmon_init_ps();
        usleep(100000);
        print_temp(AMS_PS_SYSMON_TEMPERATURE,     "PS temp ");
        print_temp(AMS_PS_SYSMON_MAX_TEMPERATURE, "    max ");
        print_temp(AMS_PL_SYSMON_TEMPERATURE,     "PL temp");
        print_temp(AMS_PL_SYSMON_MAX_TEMPERATURE, "    max");
        print_supply(AMS_PS_SYSMON_SUPPLY1,     "VCC_PSINTLP");
        print_supply(AMS_PS_SYSMON_MIN_SUPPLY1, "        min");
        print_supply(AMS_PS_SYSMON_MAX_SUPPLY1, "        max");
        print_supply(AMS_PS_SYSMON_SUPPLY2,  "VCC_FPINTFP");
        print_supply(AMS_PS_SYSMON_SUPPLY3,  "VCC_PSAUX");
        print_supply(AMS_PS_SYSMON_SUPPLY4,  "VCC_PSDDR");
        print_supply6(AMS_PS_SYSMON_SUPPLY5, "VCCO_501");
        print_supply6(AMS_PS_SYSMON_SUPPLY6, "VCCO_502");
        print_supply(AMS_PL_SYSMON_SUPPLY1,  "VCCINT");
        print_supply(AMS_PL_SYSMON_MIN_SUPPLY1,  "   min");
        print_supply(AMS_PL_SYSMON_MAX_SUPPLY1,  "   max");
        print_supply(AMS_PL_SYSMON_SUPPLY2, "VCCAUX");
        print_supply(AMS_PL_SYSMON_SUPPLY3, "VCCBRAM");
        print_supply_som(AMS_PL_SYSMON_VP_VN, "VINSOM");

	return XST_SUCCESS;
}
