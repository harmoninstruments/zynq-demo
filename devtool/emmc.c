/******************************************************************************
* Copyright (c) 2013 - 2022 Xilinx, Inc.  All rights reserved.
* Copyright (c) 2022 Advanced Micro Devices, Inc. All Rights Reserved.
* Copyright (c) 2024 Harmon Instruments, LLC
* SPDX-License-Identifier: MIT
******************************************************************************/
// doc for ff at http://elm-chan.org/fsw/ff/00index_e.html

#include "xsdps.h"
#include "xil_printf.h"
#include "ff.h"
#include "xplatform_info.h"

// these are fairly large, shared by all FAT functions
// none of these functions are thread safe
static FATFS fatfs;
static FIL fil;
MKFS_PARM mkfs_parm;

FRESULT list_dir (const char *path)
{
        FRESULT res;
        DIR dir;
        FILINFO fno;
        int nfile, ndir;
        res = f_opendir(&dir, path);
        if (res != FR_OK) {
                xil_printf("Failed to open \"%s\". (%u)\r\n", path, res);
                return res;
        }
        nfile = ndir = 0;
        for (;;) {
                res = f_readdir(&dir, &fno);
                if (res != FR_OK || fno.fname[0] == 0) break;
                if (fno.fattrib & AM_DIR) { /* Directory */
                        xil_printf("   <DIR>   %s\r\n", fno.fname);
                        ndir++;
                } else {                               /* File */
                        xil_printf("%10u %s\r\n", fno.fsize, fno.fname);
                        nfile++;
                }
        }
        f_closedir(&dir);
        xil_printf("%d dirs, %d files.\r\n", ndir, nfile);
        return res;
}

// write count bytes of data to file fn overwriting if it exists
int write_file(const char *fn, const void* data, size_t count) {
        unsigned int NumBytesWritten;
        FRESULT Res = f_mount(&fatfs, "0:/", 0);
	if (Res != FR_OK)
                goto fail1;
	Res = f_open(&fil, (char*)fn, FA_CREATE_ALWAYS | FA_WRITE);
	if (Res)
                goto fail2;
	Res = f_write(&fil, data, count, &NumBytesWritten);
	if (Res)
                goto fail3;
        Res = f_close(&fil);
        Res = f_mount(0, "", 0);  // unmount
        if (NumBytesWritten != count) {
		return XST_FAILURE;
	}
        return XST_SUCCESS;
fail3:
        f_close(&fil);
fail2:
        f_mount(0, "", 0);  // unmount
fail1:
        return XST_FAILURE;
}

// read at most count bytes of data of file fn, returns actual bytes
// read, 0 on error
size_t read_file(const char *fn, void* data, size_t count) {
        unsigned int r_actual;
        FRESULT Res = f_mount(&fatfs, "0:/", 0);
	if (Res != FR_OK)
		goto fail1;
	Res = f_open(&fil, (char*)fn, FA_READ);
	if (Res)
                goto fail2;
	Res = f_read(&fil, data, count, &r_actual);
	if (Res)
                goto fail3;
        Res = f_close(&fil);
        Res = f_mount(0, "", 0);  // unmount
        return r_actual;
fail3:
        f_close(&fil);
fail2:
        f_mount(0, "", 0);  // unmount
fail1:
        return 0;
}

/*
int FfsSdPolledExample(void)
{
	FRESULT Res;
        Res = write_file(FileName, (const void *) teststring, sizeof(teststring));

	Res = f_mount(&fatfs, "0:/", 0);
	if (Res != FR_OK) {
		return XST_FAILURE;
	}
        list_dir("0:/");
        Res = f_mount(0, "", 0);

        mkfs_parm.fmt = FM_FAT32;
	 * Path - Path to logical driver, 0 - FDISK format.
	 * 0 - Cluster size is automatically determined based on Vol size.

        //BYTE work[FF_MAX_SS];
        //Res = f_mkfs(Path, &mkfs_parm , work, sizeof work);
        //if (Res != FR_OK) {
        //	return XST_FAILURE;
        //}

        char s[128];
        size_t rcount = read_file(FileName, (void*)s, 128);
        s[127] = 0;
        xil_printf("read \"%s\" from eMMC, %d bytes\r\n", s, rcount);
	return XST_SUCCESS;
}
*/
