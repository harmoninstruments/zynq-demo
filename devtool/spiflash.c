// Copyright (C) 2024 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

// not thread safe
// not cache coherent on read DMA

#include "qspi.h" // register definitions
#include "stdint.h"
#include "xil_io.h" // register access

static inline void out32(size_t a, uint32_t d) {Xil_Out32(a,d);}
static inline uint32_t in32(size_t a) {return Xil_In32(a);}

static void gqfifo_push(uint32_t v) {
        // wait for space in FIFO TODO: timeout
        while(in32(QSPI_GQSPI_ISR) & (1<<10))
                ;
        out32(QSPI_GQSPI_GEN_FIFO, v);
}

// Generic FIFO commands
static const uint32_t gqfifo_xfer = (1<<8);
static const uint32_t gqfifo_exponent = (1<<9);
static const uint32_t gqfifo_spi = (1<<10);
static const uint32_t gqfifo_qspi = (3<<10);
static const uint32_t gqfifo_cs = (1<<12);
static const uint32_t gqfifo_bus = (1<<14);
static const uint32_t gqfifo_tx = (1<<16);
static const uint32_t gqfifo_rx = (1<<17);

static void cs_assert() {
        gqfifo_push(gqfifo_spi | gqfifo_cs | gqfifo_bus| 8);
}

static void cs_deassert() {
        gqfifo_push(gqfifo_spi | gqfifo_bus| 8);
}

static void w_byte(uint8_t b) {
        gqfifo_push(gqfifo_spi | gqfifo_bus | gqfifo_cs | gqfifo_tx | b);
}

static uint32_t r_32() {
        gqfifo_push(gqfifo_spi | gqfifo_bus | gqfifo_cs | gqfifo_rx | gqfifo_xfer | 4);
}

static uint32_t read_reg(uint8_t command) {
        cs_assert();
        w_byte(command);
        r_32();
        cs_deassert();
        while(in32(QSPI_GQSPI_ISR) & (1<<11)) // RX empty
                ;
        return in32(QSPI_GQSPI_RXD);
}

static void command_1byte(uint8_t command) {
        cs_assert();
        w_byte(command);
        cs_deassert();
}

static inline void wren() {command_1byte(0x06);}

static inline uint32_t rdsr() {
        return read_reg(0x05);
}

static inline uint32_t rdid() {
        return read_reg(0x9E);
}

static void gqfifo_push_addr_4(uint32_t addr) {
        w_byte(0xFF & (addr >> 24));
        w_byte(0xFF & (addr >> 16));
        w_byte(0xFF & (addr >> 8));
        w_byte(0xFF & addr);
}

// count must be a multiple of 4, *data must be 4 byte aligned
// cache invalidate is caller responsibility
void flash_read(uint32_t addr, uint32_t count, uint32_t *data) {
        out32(QSPI_QSPIDMA_DST_I_STS, (1<<1)); // reset DMA done
        out32(QSPI_QSPIDMA_DST_ADDR, (size_t) data);
        out32(QSPI_QSPIDMA_DST_ADDR_MSB, 0);
        out32(QSPI_QSPIDMA_DST_SIZE, count);
        out32(QSPI_GQSPI_CFG, (1<<19) | (1<<3) | (1<<31)); // enable DMA
        cs_assert();
        w_byte(0x6C); // 4 byte addressed quad output fast read
        gqfifo_push_addr_4(addr);
        w_byte(0); // dummy
        // request power of 2 sized pieces for all bytes over 255
        for(int i=8; i<26; i++) {
                if(count & (1<<i)) {
                        gqfifo_push(gqfifo_qspi | gqfifo_bus | gqfifo_cs |
                                    gqfifo_rx | gqfifo_xfer | gqfifo_exponent | i);
                }
        }
        if(count & 0xFC) { // possibly up to 252 bytes remain, no exponent
                gqfifo_push(gqfifo_qspi | gqfifo_bus | gqfifo_cs | gqfifo_rx | gqfifo_xfer | (count & 0xFC));
        }
        cs_deassert();
        while(1) {
                uint32_t v = in32(QSPI_QSPIDMA_DST_I_STS);
                if(v & (1<<1))
                        break;
        }
        out32(QSPI_GQSPI_CFG, (1<<19) | (1<<3) | (0<<31)); // disable DMA
}

static void wait_ready() {
        while(1) {
                int v = rdsr();
                if((v & 1) == 0)
                        break;
        }
}

void flash_erase_sector(uint32_t addr) {
        wren();
        cs_assert();
        w_byte(0xD8); // erase sector
        gqfifo_push_addr_4(addr);
        cs_deassert();
        wait_ready();
}

static void write_volatile_config() {
        cs_assert();
        w_byte(0x81);
        w_byte(0x83); // 8 cycle dummy
        cs_deassert();
}

// write a page (256 bytes)
void flash_page_program(uint32_t addr, uint32_t *data) {
        wren();
        cs_assert();
        w_byte(0x32); // quad input page program
        gqfifo_push_addr_4(addr);
        gqfifo_push(gqfifo_qspi | gqfifo_bus | gqfifo_cs |
                    gqfifo_tx | gqfifo_xfer | gqfifo_exponent | 8); // 256 bytes
        // the FIFO is 64 words deep, guaranteed empty
        for(size_t i=0; i<64; i++)
                out32(QSPI_GQSPI_TXD, data[i]);
        cs_deassert();
        wait_ready();
}

int spiflash_init() {
        out32(QSPI_GQSPI_SEL, 1); // select GQSPI
        out32(QSPI_GQSPI_FIFO_CTRL, 7); // flush
        // only works with divide by 4 or more
        out32(QSPI_GQSPI_CFG, (1<<19) | (1<<3)); // WP hold
        //out32(QSPI_GQSPI_LPBK_DLY_ADJ, (1<<5)); // use loopback clock
        out32(QSPI_GQSPI_TX_THRESH, 1);
        out32(QSPI_GQSPI_RX_THRESH, 1);
        out32(QSPI_GQSPI_GF_THRESH, 1);
        out32(QSPI_GQSPI_EN_REG, 1); // enable
        out32(QSPI_QSPIDMA_DST_CTRL, 0x803FFA00);
        out32(QSPI_QSPIDMA_DST_CTRL2, 0x081BFFF8 | (0<<24)); // write through no allocate
        command_1byte(0xAB); // power up
        command_1byte(0x66); // reset enable
        command_1byte(0x99); // reset
        command_1byte(0xB7); // 4 byte address mode
        write_volatile_config();
        uint32_t id = rdid();
        wait_ready();
}
