/*
 * Copyright (C) 2016 - 2019 Xilinx, Inc. All rights reserved.
 * Copyright (C) 2024 Harmon Instruments, LLC
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "stdint.h"
#include "xilfpga.h"
#include "stdint.h"
#include "xil_cache.h" // cache invalidate

uint64_t buffer[1024*1024*8]; // 64 MiB

// lwip doesn't implement MSG_WAITALL, make as many read calls as required
static int read_all(int sd, void *b, size_t len) {
        size_t bytes_complete = 0;
        while(bytes_complete < len) {
                int rv = read(sd, b + bytes_complete, len-bytes_complete);
                if(rv <= 0) {
                        // usually socket closed
                        xil_printf("get_buffer read failed %d %d \r\n", bytes_complete, rv);
                        return -1;
                }
                bytes_complete += rv;
        }
        return len;
}

// read/write memory
// address
// params access type 1-16 8-128 bits, 0 = mmio32
// increment
// count 32 b 64 MiB limit

static struct commandstr {
        uint8_t c; // command
        uint8_t f[3]; // flags, etc
        uint32_t l; // length of following data
        uint64_t d[3]; // data, address, etc
} command;
_Static_assert(sizeof(command) == 32);

static int devtool_reg_read(int sd) {
        xil_printf("reg read at addr %.16X, %d bytes\r\n", command.d[0], command.d[1]);
        switch(command.f[0]) {
        case 1:
                buffer[0] = Xil_In8(command.d[0]);
                break;
        case 2:
                buffer[0] = Xil_In16(command.d[0]);
                break;
        case 4:
                buffer[0] = Xil_In32(command.d[0]);
                break;
        case 8:
                buffer[0] = Xil_In64(command.d[0]);
                break;
        default:
                return -1;
        }
        return write(sd, buffer, 8);
}

static int devtool_reg_write(int sd) {
        xil_printf("reg write at addr %.16X, %d bytes\r\n", command.d[0], command.d[1]);
        switch(command.f[0]) {
        case 1:
                Xil_Out8(command.d[0], command.d[1] & 0xFF);
                return 0;
        case 2:
                Xil_Out16(command.d[0], command.d[1] & 0xFFFF);
                return 0;
        case 4:
                Xil_Out32(command.d[0], command.d[1] & 0xFFFFFFFF);
                return 0;
        case 8:
                Xil_Out64(command.d[0], command.d[1]);
                return 0;
                //case 16:
                //*(volatile uint128_t *)command.d[0] = *(uint128_t *)&command.d[1];
                //return 0;
        }
        return 0;
}

static int devtool_memory_write(int sd) {
        xil_printf("mem write at addr %.16X, %d bytes\r\n", command.d[0], command.l);
        memcpy((void *) command.d[0], buffer, command.l);
        return 0;
}

static int devtool_memory_read(int sd) {
        xil_printf("mem read at addr %.16X, %d bytes\r\n", command.d[0], command.d[1]);
        return write(sd, (void *)command.d[0], command.d[1]);
}

// from spiflash.c
void flash_page_program(uint32_t addr, uint32_t *data);
void flash_erase_sector(uint32_t addr);
void flash_read(uint32_t addr, uint32_t count, uint32_t *data);

// write one 64 kiB sector
static int devtool_qspi_write(int sd) {
        uint32_t addr = command.d[0];
        xil_printf("qspi write at addr %.16X, %d bytes\r\n", command.d[0], command.l);
        if(command.l != 65536)
                return -1;
        if(addr & 0xFFFF)
                return -1;
        flash_erase_sector(addr);
        xil_printf("qspi erase complete\r\n");
        uint8_t *buf8 = (uint8_t*) buffer;
        for(size_t i=0; i<65536; i+=256) {
                flash_page_program(addr + i, (uint32_t *) &buf8[i]);
        }
        xil_printf("qspi write complete\r\n");
        return 0;
}

static int devtool_qspi_read(int sd) {
        uint32_t addr = command.d[0];
        uint32_t len = command.d[1];
        xil_printf("qspi read at addr %.16X, %d bytes\r\n", addr, len);
        if(len > sizeof(buffer))
                return -1;
        Xil_DCacheInvalidateRange((size_t) buffer, len);
        flash_read(addr, len, (uint32_t *) buffer);
        int rv = write(sd, (void *)buffer, len);
        xil_printf("qspi read complete\r\n");
        return rv;
}


// from emmc.c
size_t read_file(const char *fn, void* data, size_t count);
int write_file(const char *fn, const void* data, size_t count);

// read a file from SD or eMMC
static int devtool_read_file(int sd) {
        if(command.l != 256)
                return -1;
        char *fn = (char *) buffer;
        fn[255] = 0;
        xil_printf("reading file %s\r\n", fn);
        size_t flen = read_file(fn, buffer, sizeof(buffer));
        if(flen < 0)
                return flen;
        if(flen > sizeof(buffer))
                return -4;
        if(write(sd, &flen, 4) != 4) {
                return -5;
        }
        if(flen != 0) {
                if(write(sd, buffer, flen) != flen) {
                        return -6;
                }
        }
        return 0;
}

// write a file to SD or eMMC
// first 256 bytes is filename
static int devtool_write_file(int sd) {
        xil_printf("writing file\r\n");
        if(command.l < 256)
                return -1;
        char *fn = (char *) buffer;
        fn[255] = 0;
        xil_printf("writing file %s\r\n", fn);
        write_file(fn, &fn[256], command.l-256);
        if(write(sd, "ack", 4) != 4) {
                return -5;
        }
        return 0;
}

// configure the FPGA
// requires PMU firmware to be running
static int devtool_config_fpga(int sd) {
        xil_printf("config FPGA\r\n");
	XFpga XFpgaInstance = {0U};
	int rv = XFpga_Initialize(&XFpgaInstance);
	if (rv != XST_SUCCESS) {
                xil_printf("Initialization failed\n\r");
                write(sd, "F1 ", 4);
                return 0;
	}
	rv = XFpga_BitStream_Load(
                &XFpgaInstance,
                (UINTPTR)buffer, // pointer to bitstream
                (UINTPTR)NULL, // pointer to key
                command.l,
                XFPGA_FULLBIT_EN
                );
	if (rv != XFPGA_SUCCESS) {
                write(sd, "F2 ", 4);
                xil_printf("FPGA configuration failed %d\r\n", rv);
                return 0;
	}
        xil_printf("FPGA configuration complete\r\n");
        if(write(sd, "ack", 4) != 4) {
                return -4;
        }
        return 0;
}

static void devtool_connection(const int sd)
{
	while (1) {
                // fetch the command structure
                if (read_all(sd, &command, sizeof(command)) != sizeof(command)) {
			xil_printf("error reading command from socket, closing socket\r\n");
                        break;
		}
                if(command.l > sizeof(buffer)) {
                        xil_printf("overflow\r\n");
                        break;
                }
                xil_printf("command = %.2x, datalen = %d\r\n", command.c, command.l);
                int rv = read_all(sd, buffer, command.l);
                if (rv != command.l) {
                        break;
                }
                if(command.c == 1) {
                        if(devtool_read_file(sd) < 0)
                                break;
                } else if(command.c == 2) {
                        if(devtool_write_file(sd) < 0)
                                break;
                } else if(command.c == 3) {
                        if(devtool_config_fpga(sd) < 0)
                                break;
                } else if(command.c == 4) {
                        if(devtool_reg_read(sd) < 0)
                                break;
                } else if(command.c == 5) {
                        if(devtool_reg_write(sd) < 0)
                                break;
                } else if(command.c == 6) {
                        if(devtool_memory_read(sd) < 0)
                                break;
                } else if(command.c == 7) {
                        if(devtool_memory_write(sd) < 0)
                                break;
                } else if(command.c == 8) {
                        if(devtool_qspi_read(sd) < 0)
                                break;
                } else if(command.c == 9) {
                        if(devtool_qspi_write(sd) < 0)
                                break;
                }
        }
	close(sd);
        xil_printf("devtool socket %d closed\r\n", sd);
}

void devtool_thread()
{
        u16_t echo_port = 7;
	int sock;
#if LWIP_IPV6==0
	struct sockaddr_in address, remote;
	memset(&address, 0, sizeof(address));
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return;
	address.sin_family = AF_INET;
	address.sin_port = htons(echo_port);
	address.sin_addr.s_addr = INADDR_ANY;
#else
	struct sockaddr_in6 address, remote;
	memset(&address, 0, sizeof(address));
	address.sin6_len = sizeof(address);
	address.sin6_family = AF_INET6;
	address.sin6_port = htons(echo_port);
	memset(&(address.sin6_addr), 0, sizeof(address.sin6_addr));
	if ((sock = lwip_socket(AF_INET6, SOCK_STREAM, 0)) < 0)
		return;
#endif
	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
		return;
	lwip_listen(sock, 0);
	int size = sizeof(remote);
	while (1) {
                int sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
                if (sd > 0) {
                        devtool_connection(sd);
                        xil_printf("accept: Heap available = %d\r\n", xPortGetFreeHeapSize());
                }
	}
}
