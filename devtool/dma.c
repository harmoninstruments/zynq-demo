#include "dma_regs.h"
#include "xil_io.h"

// parameters should be checked by caller
// cache should be flushed/invalidated by caller
int dma_copy(uint64_t dst, uint64_t src, uint32_t count) {
        uint32_t base = ADMA_CH0_BASEADDR;
        Xil_Out32(base + ZDMA_CH_CTRL0, 0);
        Xil_Out64(base + ZDMA_CH_SRC_DSCR_WORD0, src);
        Xil_Out64(base + ZDMA_CH_DST_DSCR_WORD0, dst);
        Xil_Out32(base + ZDMA_CH_SRC_DSCR_WORD2, count);
        Xil_Out32(base + ZDMA_CH_DST_DSCR_WORD2, count);
        Xil_Out32(base + ZDMA_CH_SRC_DSCR_WORD3, 0);
        Xil_Out32(base + ZDMA_CH_DST_DSCR_WORD3, 0);
        // trigger the DMA run
        Xil_Out32(base + ZDMA_CH_CTRL2, 1);
        while(1) {
                uint32_t rv = Xil_In32(base + ZDMA_CH_STATUS);
                rv &= 0x3;
                if(rv == 0)
                        return 0; // success
                if(rv == 3)
                        return -1; // fail
                // else busy todo: timeout
        }
}
