#!/usr/bin/env python3
# Copyright (C) 2024 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

import socket
import time
import struct
import argparse

default_host = "10.0.0.137"

qspi_sector_size = 65536

class Devtool:
    def __init__(self, host, port=7):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, port))
    def recvn(self, n):
        rv = self.s.recv(n)
        while len(rv) != n:
            rv += self.s.recv(n - len(rv))
        return rv
    def read_file(self, fn):
        assert len(fn) < 256
        d = struct.pack("BBBBI", 1, 0, 0, 0, 256)
        d += bytes(24)
        d += bytes(fn, 'utf-8')
        d += bytes((256+32)-len(d))
        self.s.sendall(d)
        l = self.recvn(4)
        l, = struct.unpack("I", l)
        print(f"{l} bytes")
        d = self.recvn(l)
        return d
    def save_file(self, fn_zynq, fn_local):
        d = self.read_file(fn_zynq)
        with open(fn_local, "wb") as f:
            f.write(d)
    def write_file_data(self, fn, data):
        assert len(fn) < 56
        d = struct.pack("BBBBI", 2, 0, 0, 0, len(data)+256)
        d += bytes(24)
        d += bytes(fn, 'utf-8')
        d += bytes(256+32-len(d))
        print(d)
        self.s.sendall(d)
        self.s.sendall(data)
        assert self.recvn(4) == b"ack\x00"
    def write_file(self, fn):
        with open(fn, "rb") as f:
            data = f.read()
        self.write_file_data(fn, data)
    def write_qspi_sector(self, addr, data):
        assert addr & (qspi_sector_size - 1) == 0
        assert len(data) == qspi_sector_size
        d = struct.pack("BBBBIQQQ", 9, 0, 0, 0, qspi_sector_size, addr, 0 ,0)
        self.s.sendall(d+data)
    def write_qspi(self, addr, data):
        assert addr + len(data) < (64*1024*1024)
        assert addr >= 0
        extended_length = qspi_sector_size*((len(data)+qspi_sector_size-1)//qspi_sector_size)
        extension_length = extended_length - len(data)
        print(extension_length)
        data += bytes(extension_length)
        print(len(data))
        for i in range(len(data)//qspi_sector_size):
            self.write_qspi_sector(addr + i*qspi_sector_size, data[i*qspi_sector_size:(i+1)*qspi_sector_size])
    def write_file_qspi(self, fn, addr):
        with open(fn, "rb") as f:
            data = f.read()
        self.write_qspi(addr, data)
        readback = self.read_qspi(addr, len(data))
        assert readback == data
    def read_qspi(self, addr, length):
        assert addr + length < (64*1024*1024)
        assert addr >= 0
        d = struct.pack("BBBBIQQQ", 8, 0, 0, 0, 0, addr, length, 0)
        self.s.sendall(d)
        d = self.recvn(length)
        return d

    def config_fpga(self, fn):
        with open(fn, "rb") as f:
            bitstream = f.read()
        d = struct.pack("BBBBI", 3, 0, 0, 0, len(bitstream))
        self.s.sendall(d+bytes(24))
        self.s.sendall(bitstream)
        assert self.recvn(4) == b"ack\x00"
    def read_16(self, addr):
        assert (addr & 1) == 0
        d = struct.pack("BBBBIQQQ", 4, 2, 0, 0, 0, addr, 0, 0)
        self.s.sendall(d)
        l, = struct.unpack("Q", self.recvn(8))
        print(f"0x{l:04x}")
        return l
    def read_32(self, addr):
        assert (addr & 3) == 0
        d = struct.pack("BBBBIQQQ", 4, 4, 0, 0, 0, addr, 0, 0)
        self.s.sendall(d)
        l, = struct.unpack("Q", self.recvn(8))
        print(f"0x{l:08x}")
        return l
    def read_64(self, addr):
        assert (addr & 7) == 0
        d = struct.pack("BBBBIQQQ", 4, 8, 0, 0, 0, addr, 0, 0)
        self.s.sendall(d)
        l, = struct.unpack("Q", self.recvn(8))
        print(f"0x{l:016x}")
        return l

    def write_reg_64(self, addr, d):
        assert (addr & 7) == 0
        d = struct.pack("BBBBIQQQ", 4, 8, 0, 0, 0, addr, d, 0)
        self.s.sendall(d)
        l, = struct.unpack("Q", self.recvn(8))
        print(f"0x{l:016x}")
        return l

    # returns count bytes
    def read_memory(self, addr, count):
        self.s.sendall(struct.pack("BBBBIQQQ", 6, 0, 0, 0, 0, addr, count, 0))
        return self.recvn(count)

    # data is bytes()
    def write_memory(self, addr, data):
        self.s.sendall(struct.pack("BBBBIQQQ", 7, 0, 0, 0, len(data), addr, 0, 0))
        self.s.sendall(data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument('--read32', type=str, help="address to read a 32 bit register or memory", default="0x0")
    parser.add_argument('--host', type=str, help="ip or hostname of device", default=default_host)
    args = parser.parse_args()
    d = Devtool(args.host)

    #print(d.read_file("Test2.bin"))
    #print(d.read_file("boot_script.sh"))
    #print(d.read_file("Test2.bin"))
    #d.write_file_data("Test2.bin", b"this is a test, written from Python, updated")
    #d.write_file("boot.bin")
    #d.save_file("boot.bin", "b.bin")
    print(d.config_fpga("hi_kria.bit"))
    #print(d.read_file("Test2.bin"))
    d.read_file("boot.bin")
    #print(d.read_file("boot_script.sh"))
    for i in range(10):
        d.read_32(4*i)
    for i in range(10):
        d.read_64(0x80000000 + 8*i)
    for i in range(10):
        d.read_64(0x800000000 + 8*i)
