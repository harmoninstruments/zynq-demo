# Copyright 2014 - 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import queue
from AXI.Stream import AXIStream, dotdict

class AXIBus():
    def __init__(self, name, abits=32, dbits=32, idbits=6, is_HP=False):
        self.abits = abits
        self.dbits = dbits
        self.idbits = idbits
        self.is_HP = is_HP
        self.clk = Signal()
        addr_u = [('addr', abits), ('burst', 2), ('cache', 4), ('id', idbits),
                  ('len', 4), ('lock', 2), ('prot', 3), ('qos', 4), ('size', 2)]
        # address
        self.ar = AXIStream(name+'ar', addr_u, [])
        self.aw = AXIStream(name+'aw', addr_u, [])
        # write response
        self.b = AXIStream(name+'b', [('id', self.idbits), ('resp', 2)], [])
        data_u = [('data', dbits), ('id', idbits), ('last', 1)]
        # read data
        self.r = AXIStream(name+'r', data_u + [('resp', 2)], [])
        self.w = AXIStream(name+'w', data_u + [('strb', dbits//8)], [])

        if False and is_HP:
            b.fifo = {}
            b.fifo.racount = Signal(), # fixme, sizes
            b.fifo.rcount = Signal(),
            b.fifo.rdissuecapen = Signal(reset=0)
            b.fifo.wrcount = Signal(), # fixme, sizes
            b.fifo.wcount = Signal(),
            b.fifo.wrissuecapen = Signal(reset=0)

    def get_zynq_ports(self, name, zynq_is_master=True):
        d = {
            'i_{}ACLK'.format(name):self.clk,
            **self.ar.get_zynq_ports(name+"AR", zynq_is_master),
            **self.aw.get_zynq_ports(name+"AW", zynq_is_master),
            **self.b.get_zynq_ports(name+"B", not zynq_is_master),
            **self.r.get_zynq_ports(name+"R", not zynq_is_master),
            **self.w.get_zynq_ports(name+"W", zynq_is_master),
        }
        return d

class AXIBus_M_test(AXIBus):
    def sim_processes(self, sim, rw = 'rw'):
        axi = self
        if 'w' in rw:
            self.q_aw = axi.aw.sim_queue = queue.Queue()
            self.q_w = axi.aw.sim_queue = queue.Queue()
            self.q_b = axi.b.sim_queue = queue.Queue()
            sim.add_sync_process(axi.aw.sim_writer)
            sim.add_sync_process(axi.w.sim_writer)
            sim.add_sync_process(axi.b.sim_reader)
        if 'r' in rw:
            self.q_ar = axi.ar.sim_queue = queue.Queue()
            self.q_r = axi.r.sim_queue = queue.SimpleQueue()
            sim.add_sync_process(axi.ar.sim_writer)
            sim.add_sync_process(axi.r.sim_reader)
        self.wid = 3
        self.rid = 7
        return sim
    def write(self, addr, data):
        self.q_aw.put({'addr':addr, 'id':self.wid,})
        self.wid += 1
        for x in data[:-1]:
            self.q_w.put({'data':x, 'last':0})
        for x in [data[-1]]:
            self.q_w.put({'data':x, 'last':1})
    def read_expect(self, addr, count):
        self.q_ar.put({'addr':addr, 'id':self.rid, 'len':count-1})
        self.rid += 1
        return self.rid-1
    def get_read_data(self, rid, count):
        rv = []
        while True:
            try:
                d = self.q_r.get(True)
            except GeneratorExit:
                return
            except:
                continue
            print("r", d)
            assert d['id'] == rid
            rv.append(d['data'])
            if d['last']:
                break
        return rv

    def read(self, addr, count):
        rid = self.req_read(addr, count)
        return self.get_read_data(rid, count)

class AXI4Bus():
    def __init__(self, name, abits=32, dbits=32, idbits=6, lenbits=8, lockbits=1, sizebits=2, userbits=16, is_HP=False, clk="sync"):
        self.abits = abits
        self.dbits = dbits
        self.idbits = idbits
        self.is_HP = is_HP
        self.clk = clk
        addr_u = [('addr', abits), ('burst', 2), ('cache', 4), ('id', idbits),
                  ('len', lenbits), ('lock', lockbits), ('prot', 3), ('qos', 4), ('size', sizebits),
                  ('user', userbits)
                  ]
        # address
        self.ar = AXIStream(name+'ar', addr_u, [])
        self.aw = AXIStream(name+'aw', addr_u, [])
        # write response
        self.b = AXIStream(name+'b', [('id', self.idbits), ('resp', 2)], [])
        data_u = [('data', dbits), ('id', idbits), ('last', 1)]
        # read data
        self.r = AXIStream(name+'r', data_u + [('resp', 2)], [])
        self.w = AXIStream(name+'w', [('data', dbits), ('last', 1), ('strb', dbits//8)], [])

        if False and is_HP:
            b.fifo = {}
            b.fifo.racount = Signal(), # fixme, sizes
            b.fifo.rcount = Signal(),
            b.fifo.rdissuecapen = Signal(reset=0)
            b.fifo.wrcount = Signal(), # fixme, sizes
            b.fifo.wcount = Signal(),
            b.fifo.wrissuecapen = Signal(reset=0)

    def get_zynq_ports(self, name, zynq_is_master=True):
        d = {
            #'i_{}ACLK'.format(name):self.clk,
            **self.ar.get_zynq_ports(name+"ar", zynq_is_master),
            **self.aw.get_zynq_ports(name+"aw", zynq_is_master),
            **self.b.get_zynq_ports(name+"b", not zynq_is_master),
            **self.r.get_zynq_ports(name+"r", not zynq_is_master),
            **self.w.get_zynq_ports(name+"w", zynq_is_master),
        }
        return d
