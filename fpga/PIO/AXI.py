# AXI PIO class - manages registers and memory regions
# Copyright 2019-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.utils import log2_int
from PIO.Generic import PIO_Generic
from AXI.PIO_write import AXI_PIO_write
from AXI.PIO_read import AXI_PIO_read
from AXI.Stream import AXIStream

class PIO_AXI(PIO_Generic):
    def __init__(self, axi):
        dbits = len(axi.r.data)
        alow = log2_int(dbits // 8)
        abits = len(axi.ar.addr) - alow
        self.base_init(name="AXI", abits=abits, dbits=dbits)
        self.axi = axi
        self.wlocal = AXIStream(name="wlocal", u=[("data", dbits), ("addr", len(self.waddr))])
        self.wstreams = []
        self.clk = axi.clk

    def elaborate(self, platform):
        m = Module()
        axi = self.axi

        bytes_word = 16

        axir = m.submodules.axi_pio_read = AXI_PIO_read(
            axi=axi, a=self.raddr, d=self.rdata, v=self.rvalid, r=self.rready)

        axiw = m.submodules.axi_pio_write = AXI_PIO_write(
            axi, local=self.wlocal)

        m.d.comb += [
            self.waddr.eq(self.wlocal.addr),
            self.wdata.eq(self.wlocal.data),
            self.wvalid.eq(self.wlocal.valid),
            self.wlocal.ready.eq(1),
        ]

        for s in self.wstreams:
            (a,d) = s
            ahigh = a // (2**(2+len(d.addr)))
            print("Write AXI stream {:08x} {:08X} {}".format(a, ahigh, d))
            m.d.comb += [
                d.data.eq(self.wlocal.data),
                d.addr.eq(self.wlocal.addr[:len(d.addr)]),
                d.valid.eq(self.wlocal.valid & (self.wlocal.addr[len(d.addr):] == ahigh)),
            ]

        with m.Switch(self.wlocal.addr):
            for (a,d) in self.wstreams:
                casestr = self.bin_masked(a//bytes_word, len(self.wlocal.addr), lsb=len(d.addr))
                print("Write AXI stream {:08x} {}".format(a, casestr))
                with m.Case(casestr):
                    m.d.comb += self.wlocal.ready.eq(d.ready)
            with m.Case():
                m.d.comb += self.wlocal.ready.eq(1)

        m = self.base_elaborate(m)
        return m

    def add_wstream(self, addr, stream):
        self.wstreams.append((addr, stream))
