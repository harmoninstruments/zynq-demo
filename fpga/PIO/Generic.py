# Generic PIO class - manages registers and memory regions
# Copyright 2019-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class PIO_Addr():
    def __init__(self, addr, data, lsb = 0, bits=1, timeout=0, ack=None, name="undefined"):
        self.addr = addr
        self.data = data
        self.lsb = lsb
        self.bits = bits
        self.timeout = timeout
        self.ack = ack
        self.name = name

class PIO_Generic(Elaboratable):
    def base_init(self, name='AXI', abits=20, dbits=32):
        self.name = name
        self.wvalid = Signal()
        self.rvalid = Signal()
        self.waddr = Signal(abits)
        self.raddr = Signal(abits)
        self.readcases = []
        self.wvalidcases = []
        self.wocases = []
        self.wdata = Signal(dbits)
        self.rdata = Signal(dbits)
        self.rready = Signal()
        self.bytes_wide = dbits//8

    def bin_masked(self, a, bits, lsb=0):
        b = bin(a)[2:]
        b = '0'*(len(self.waddr)-len(b)) + b
        if lsb != 0:
            b = b[:-lsb] + '-'*lsb
        return b

    def add_ro_reg(self, addr, data, lsb=0, timeout=0, ack=None, name="undefined"):
        self.readcases += [PIO_Addr(addr, data, lsb, timeout=timeout, ack=ack, name=name)]
        return self.get_rvalid(addr, lsb, name=name)
    def add_wvalid(self, addr, data, lsb=0, name="undefined"):
        self.wvalidcases += [PIO_Addr(addr, data, lsb, name=name)]
    def add_wo_reg(self, addr, data, name="undefined"):
        self.wocases += [PIO_Addr(addr, data, name=name)]
        return self.get_wvalid(addr, name=name)
    def add_rw_reg(self, addr, data, name="undefined"):
        self.add_wo_reg(addr, data, name=name)
        self.add_ro_reg(addr, data, name=name)
    def get_rvalid(self, addr, lsb=0, name="undefined"):
        print(f"rvalid: {addr:08X}, {lsb}, {name}")
        return (self.rvalid & (self.raddr[lsb:] == C(addr//self.bytes_wide)[lsb:]))
    def get_wvalid(self, addr, lsb=0, quiet=False, name="undefined"):
        if not quiet:
            print(f"wvalid: {addr:08X}, {lsb}, {name}")
        return (self.wvalid & (self.waddr[lsb:] == C(addr//self.bytes_wide)[lsb:]))

    def base_elaborate(self, m):
        clk = self.clk
        print("base_elaborate", self.name)
        rvalid_del = Signal()

        m.d[clk] += rvalid_del.eq(self.rvalid)

        timer = Signal(10)
        timed_out = Signal()

        m.d.comb += timed_out.eq((timer == 1023) & (~self.rready))

        with m.If(self.rvalid):
            m.d[clk] += timer.eq(1)
        with m.Elif(self.rready):
            m.d[clk] += timer.eq(0)
        with m.Elif(timer != 0):
            m.d[clk] += timer.eq(timer+1)

        with m.Switch(self.raddr):
            for c in self.readcases:
                print(f"roreg: {c.addr:04X}, {c.data}, {c.lsb}, {c.name}")
                with m.Case(self.bin_masked(c.addr//self.bytes_wide, len(self.raddr), lsb=c.lsb)):
                    m.d[clk] += self.rdata.eq(c.data)
                    m.d[clk] += self.rready.eq(self.rvalid if (c.ack is None) else (c.ack | timed_out))
            with m.Case():
                m.d[clk] += self.rdata.eq(0)
                m.d[clk] += self.rready.eq(self.rvalid)

        for c in self.wvalidcases:
            print("wvalid: {:08X}, {}, {}".format(c.addr, c.data, c.lsb))
            m.d.comb += c.data.eq(self.get_wvalid(c.addr, c.lsb, quiet=True))

        for c in self.wocases:
            print(f"woreg: {c.addr:08X}, {c.data}, {len(c.data)}, {c.name}")
            with m.If(self.get_wvalid(c.addr, c.lsb, quiet=True)):
                m.d[clk] += c.data.eq(self.wdata[:len(c.data)])

        return m
