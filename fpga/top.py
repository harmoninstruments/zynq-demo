#!/usr/bin/env python3
# Copyright (C) 2022-2024 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import argparse

from amaranth.build import *
from amaranth import *

from board_kria_k26 import KriaK26Platform

from AXI.Bus import AXI4Bus
from PIO.AXI import PIO_AXI

class KriaPS(Elaboratable):
    def elaborate(self, platform):
        m = Module()
        m.domains.sync = ClockDomain("sync", reset_less=True) # 250 MHz main clock from PS
        m.domains.clk325 = ClockDomain("clk325", reset_less=True) # 325 MHz from PS clock

        axi_pio0 = AXI4Bus(
            "PIO0_", abits=40, dbits=128, idbits=16, lenbits=8, lockbits=1, sizebits=3,
            userbits=16, clk="sync")
        axi_pio1 = AXI4Bus(
            "PIO1_", abits=40, dbits=128, idbits=16, lenbits=8, lockbits=1, sizebits=3,
            userbits=16, clk="sync")
        axi_hp0 = AXI4Bus(
            "HP0_", abits=49, dbits=128, idbits=6, lenbits=8, lockbits=1, sizebits=3,
            userbits=1, clk="sync")
        pio = PIO_AXI(axi_pio1) # PIO bus in the 250 MHz clock domain

        led = platform.request("led", 0, dir='-')

        count250 = Signal(62)
        m.d.sync += count250.eq(count250 + 1)
        m.d.sync += led.eq(count250[26])

        interrupt = Signal()

        clk250_raw = Signal()
        clk325_raw = Signal()

        m.submodules.bd = Instance(
            "bd_ps",
            i_maxihpm0_fpd_aclk_0 = ClockSignal(),
            i_maxihpm0_lpd_aclk_0 = ClockSignal(),
            i_saxihp0_fpd_aclk_0 = ClockSignal(),
            i_pl_ps_irq0_0 = interrupt,
            o_pl_clk0_0 = clk250_raw, # 250 MHz out
            o_pl_clk1_0 = clk325_raw, # 325 MHz out
            **axi_pio0.get_zynq_ports("M_AXI_HPM0_FPD_0_"),
            **axi_pio1.get_zynq_ports("M_AXI_HPM0_LPD_0_"),
            **axi_hp0.get_zynq_ports("S_AXI_HP0_FPD_0_", zynq_is_master=False),
        )

        m.submodules.bufg_250 = Instance("BUFG_PS", i_I=clk250_raw, o_O=ClockSignal())
        m.submodules.bufg_325 = Instance("BUFG_PS", i_I=clk325_raw, o_O=ClockSignal("clk325"))

        pio.add_ro_reg(0x80000000, C(0x0123456789ABCDEF0C0FFEECAFED00D),
                       name="a read only 128 bit register filled with nonsense")
        r10 = Signal(32, reset=0x12345678)
        pio.add_rw_reg(0x80000010, r10, name="32 bit read/write test register")
        pio.add_ro_reg(0x80000020, Cat(C(0,2), count250),
                       name="64 bit read only nanoseconds since configuration")
        pio.add_ro_reg(0x81000000, pio.raddr, lsb=20, name="address echo")

        m.submodules.pio0 = pio

        return m

JA1_io_hi_kria = [
    Resource(
        "led", 0, Pins("D18", dir='o', conn=("som", 1)),
        Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4")),
]

if __name__ == "__main__":
    p = KriaK26Platform()
    # this brings in the block design from the separate project we created
    # the ../ on the below is because it is relative to the Amaranth created build directory
    p.tcl_after_read += "add_files -norecurse ../../build_bd/demo.srcs/sources_1/bd/bd_ps/bd_ps.bd\n"

    p.tcl_after_read += "launch_runs synth_1 -jobs 16"
    p.tcl_after_synth += "launch_runs impl_1 -jobs 16"
    #p.constraints += "create_clock -period 2.0 -name gt500 [get_ports clk500_gth_0__p]\n"
    parser = argparse.ArgumentParser()
    parser.add_argument('--board', type=str, help="target board", default="kr260")
    args = parser.parse_args()

    if args.board == "hi-kria":
        p.add_resources(JA1_io_hi_kria)

    p.build(
        KriaPS(),
        name='demo',
        do_program=False,
        verbose=True,
        do_build=True)
