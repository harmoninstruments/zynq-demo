create_project demo build_bd -part xck26-sfvc784-2LV-c
set_property board_part xilinx.com:kv260_som:part0:1.4 [current_project]
set_property board_connections {som240_1_connector xilinx.com:kv260_carrier:som240_1_connector:1.3} [current_project]
create_bd_design "bd_ps"
update_compile_order -fileset sources_1

create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e zynq_ultra_ps_e_0

apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]

set_property -dict [list \
  CONFIG.PSU__CRL_APB__PL0_REF_CTRL__FREQMHZ {250} \
  CONFIG.PSU__FPGA_PL1_ENABLE {0} \
  CONFIG.PSU__USE__M_AXI_GP1 {0} \
  CONFIG.PSU__USE__S_AXI_GP0 {1} \
  CONFIG.PSU__AFI0_COHERENCY {1} \
  CONFIG.PSU__PROTECTION__ENABLE {0} \
] [get_bd_cells zynq_ultra_ps_e_0]
startgroup
make_bd_pins_external  [get_bd_cells zynq_ultra_ps_e_0]
make_bd_intf_pins_external  [get_bd_cells zynq_ultra_ps_e_0]
endgroup
generate_target all [get_files bd_ps.bd]
catch { config_ip_cache -export [get_ips -all bd_ps_zynq_ultra_ps_e_0_0] }
export_ip_user_files -of_objects [get_files bd_ps.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] bd_ps.bd]
launch_runs bd_ps_zynq_ultra_ps_e_0_0_synth_1 -jobs 16
wait_on_run bd_ps_zynq_ultra_ps_e_0_0_synth_1
write_hw_platform -fixed -force -file hi_kria.xsa
close_bd_design [get_bd_designs bd_ps]
close_project
exit
