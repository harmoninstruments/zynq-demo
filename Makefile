devtool.bin:
	make -C devtool/bsp/
	make -C devtool/
	aarch64-none-elf-objcopy -O binary devtool/executable.elf devtool.bin
	aarch64-none-elf-size devtool/executable.elf

.PHONY: devtool.bin

clean:
	rm -f *~ vivado*.jou vivado*.log
